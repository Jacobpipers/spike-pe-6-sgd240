# Spike Report

## SPIKE PE 6 - Cloth Simulation

### Introduction

Clothing is often simulated in a realistic fashion in modern games. How can Unreal Engine help us do it?


### Goals

* The Spike Report should answer each of the Gap questions

Building on Spike PE-5

* Create a new type of Target which has Cloth elements
* See how you can combine a target which includes both Cloth and Destructible elements.


### Personnel

In this section, list the primary author of the spike report, as well as any personnel who assisted in completing the work.

* Primary - Jacob Pipers
* Secondary - N/A

### Technologies, Tools, and Resources used

Provide resource information for team members who wish to learn additional information from this spike.

* [Cloth Documentation](https://docs.unrealengine.com/latest/INT/Engine/Physics/Cloth/Overview/)
* Visual Studio
* Unreal Engine
* Maya


### Tasks Undertaken

Show only the key tasks that will help the developer/reader understand what is required.

* Create Mesh In 3d Software (Maya/Blender).
* Import Mesh as Skeleton mesh
* Follow this [Guide](https://docs.unrealengine.com/latest/INT/Engine/Physics/Cloth/Overview/)


### What we found out

Tech: How does Unreal Engine handle Cloth Simulation?

Unreal Uses NVIDIA's NvCloth solver. You paint the weight of diffrent points on the mesh to simulate cloth.

### [Optional] Open Issues/Risks

List out the issues and risks that you have been unable to resolve at the end of the spike. You may have uncovered a whole range of new risks as well.

### [Optional] Recommendations

You may state that another spike is required to resolve new issues identified (or) indicate that this spike has increased the team�s confidence in XYZ and move on.