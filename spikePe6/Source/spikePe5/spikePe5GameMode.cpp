// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "spikePe5.h"
#include "spikePe5GameMode.h"
#include "spikePe5HUD.h"
#include "spikePe5Character.h"

AspikePe5GameMode::AspikePe5GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AspikePe5HUD::StaticClass();
}
