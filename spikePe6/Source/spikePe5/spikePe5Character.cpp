// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "spikePe5.h"
#include "spikePe5Character.h"
#include "spikePe5Projectile.h"
#include "Animation/AnimInstance.h"
#include "GameFramework/InputSettings.h"
#include "Kismet/HeadMountedDisplayFunctionLibrary.h"
#include "MotionControllerComponent.h"
#include "Classes/PhysicsEngine/RadialForceComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include <EngineGlobals.h>
#include <Runtime/Engine/Classes/Engine/Engine.h>
#include <Runtime/Engine/Classes/Components/DestructibleComponent.h>

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// AspikePe5Character

AspikePe5Character::AspikePe5Character()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->RelativeLocation = FVector(-20.56f, 1.75f, 64.f); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->RelativeRotation = FRotator(1.9f, -19.19f, 5.2f);
	Mesh1P->RelativeLocation = FVector(-0.5f, -4.4f, -155.7f);

	// Create a gun mesh component
	FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	FP_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	FP_Gun->bCastDynamicShadow = false;
	FP_Gun->CastShadow = false;
	// FP_Gun->SetupAttachment(Mesh1P, TEXT("GripPoint"));
	FP_Gun->SetupAttachment(RootComponent);

	FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	FP_MuzzleLocation->SetupAttachment(FP_Gun);
	FP_MuzzleLocation->SetRelativeLocation(FVector(0.2f, 48.4f, -10.6f));

	// Default offset from the character location for projectiles to spawn
	GunOffset = FVector(100.0f, 0.0f, 10.0f);

	// Note: The ProjectileClass and the skeletal mesh/anim blueprints for Mesh1P, FP_Gun, and VR_Gun 
	// are set in the derived blueprint asset named MyCharacter to avoid direct content references in C++.

	// Create VR Controllers.
	R_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("R_MotionController"));
	R_MotionController->Hand = EControllerHand::Right;
	R_MotionController->SetupAttachment(RootComponent);
	L_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("L_MotionController"));
	L_MotionController->SetupAttachment(RootComponent);

	// Create a gun and attach it to the right-hand VR controller.
	// Create a gun mesh component
	VR_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("VR_Gun"));
	VR_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	VR_Gun->bCastDynamicShadow = false;
	VR_Gun->CastShadow = false;
	VR_Gun->SetupAttachment(R_MotionController);
	VR_Gun->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));

	VR_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("VR_MuzzleLocation"));
	VR_MuzzleLocation->SetupAttachment(VR_Gun);
	VR_MuzzleLocation->SetRelativeLocation(FVector(0.000004, 53.999992, 10.000000));
	VR_MuzzleLocation->SetRelativeRotation(FRotator(0.0f, 90.0f, 0.0f));		// Counteract the rotation of the VR gun model.


	// Radial Force 
	FirstPersonRadialForceComponent = CreateDefaultSubobject<URadialForceComponent>(TEXT("RadialForceComponent"));
	FirstPersonRadialForceComponent->SetupAttachment(RootComponent);

	ForceFieldDisplay = CreateDefaultSubobject<USphereComponent>(TEXT("RadialDisplay"));
	ForceFieldDisplay->SetupAttachment(FirstPersonRadialForceComponent);
	ForceFieldDisplay->SetMaterial(0,ShieldMat);
	ForceFieldDisplay->SetHiddenInGame(true, false);

	// Uncomment the following line to turn motion controllers on by default:
	//bUsingMotionControllers = true;
}

void AspikePe5Character::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Attach gun mesh component to Skeleton, doing it here because the skeleton is not yet created in the constructor
	FP_Gun->AttachToComponent(Mesh1P, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));

	// Show or hide the two versions of the gun based on whether or not we're using motion controllers.
	if (bUsingMotionControllers)
	{
		VR_Gun->SetHiddenInGame(false, true);
		Mesh1P->SetHiddenInGame(true, true);
	}
	else
	{
		VR_Gun->SetHiddenInGame(true, true);
		Mesh1P->SetHiddenInGame(false, true);
	}
}

//////////////////////////////////////////////////////////////////////////
// Input

void AspikePe5Character::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	//InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AspikePe5Character::TouchStarted);
	if (EnableTouchscreenMovement(PlayerInputComponent) == false)
	{
		PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AspikePe5Character::OnFire);
		PlayerInputComponent->BindAction("RightHandAttack", IE_Pressed, this, &AspikePe5Character::StartRadialFire);
		PlayerInputComponent->BindAction("RightHandAttack", IE_Released, this, &AspikePe5Character::FireRadialFire);
		PlayerInputComponent->BindAction("PowerThrust", IE_Pressed, this, &AspikePe5Character::EnableThrust);
		PlayerInputComponent->BindAction("PowerThrust", IE_Released, this, &AspikePe5Character::DisableThrust);

		PlayerInputComponent->BindAction("One", IE_Pressed, this, &AspikePe5Character::Weapon1);
		PlayerInputComponent->BindAction("Two", IE_Pressed, this, &AspikePe5Character::Weapon2);
		PlayerInputComponent->BindAction("Three", IE_Pressed, this, &AspikePe5Character::Weapon3);
		PlayerInputComponent->BindAction("WeaponChangeup", IE_Pressed, this, &AspikePe5Character::ChangeWeapon);
		PlayerInputComponent->BindAction("WeaponChangeDown", IE_Pressed, this, &AspikePe5Character::ChangeWeaponDown);

	}


	// Movement and VR
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AspikePe5Character::OnResetVR);
	PlayerInputComponent->BindAxis("MoveForward", this, &AspikePe5Character::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AspikePe5Character::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AspikePe5Character::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AspikePe5Character::LookUpAtRate);
}

void AspikePe5Character::ChangeWeapon()
{
	// Change Camera
	switch (CurrentWeapon)
	{
	case WeaponTypeEnum::None:
		ChangeToForcePush();
		break;
	case WeaponTypeEnum::Laser:
		ChangeToNone();
		break;
	case WeaponTypeEnum::Riffle:
		ChangeToLaser();
		break;
	case WeaponTypeEnum::ForcePush:
		ChangeToRiffle();
		break;
	default:
		break;
	}
}

void AspikePe5Character::ChangeWeaponDown()
{
	// Change Camera
	switch (CurrentWeapon)
	{
	case WeaponTypeEnum::None:
		ChangeToLaser();
		break;
	case WeaponTypeEnum::Laser:
		ChangeToRiffle();
		break;
	case WeaponTypeEnum::Riffle:
		ChangeToForcePush();
		break;
	case WeaponTypeEnum::ForcePush:
		ChangeToNone();
		break;
	default:
		break;
	}
}

void AspikePe5Character::Weapon3()
{
	ChangeToForcePush();
}

void AspikePe5Character::Weapon2()
{
	ChangeToRiffle();
}

void AspikePe5Character::Weapon1()
{
	ChangeToLaser();
}


void AspikePe5Character::ChangeToNone()
{
	CurrentWeapon = WeaponTypeEnum::None;
	FP_Gun->SetHiddenInGame(true, true);
	FP_Gun->SetSkeletalMesh(Guns["None"].GetGunSkeleton(), true);
}

void AspikePe5Character::ChangeToForcePush()
{
	CurrentWeapon = WeaponTypeEnum::ForcePush;
	FP_Gun->SetHiddenInGame(false, true);
	FP_Gun->SetSkeletalMesh(Guns["ForcePush"].GetGunSkeleton(), true);

}

void AspikePe5Character::ChangeToRiffle()
{
	CurrentWeapon = WeaponTypeEnum::Riffle;
	FP_Gun->SetHiddenInGame(false, true);
	FP_Gun->SetSkeletalMesh(Guns["Riffle"].GetGunSkeleton(), true);
}

void AspikePe5Character::ChangeToLaser()
{
	CurrentWeapon = WeaponTypeEnum::Laser;
	FP_Gun->SetHiddenInGame(false, true);
	FP_Gun->SetSkeletalMesh(Guns["Laser"].GetGunSkeleton(), true);
}


void AspikePe5Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AspikePe5Character::ForceFieldGrow(DeltaTime);
}

void AspikePe5Character::ForceFieldGrow(float DeltaTime)
{
	if (HasSheildGrowingStarted)
	{
		ForceFieldDisplay->SetHiddenInGame(false, true);
		if (isForceSheildIsGrowing)
		{
			if (IsThrustEnabled)
			{
				ForceShieldRadius += (ForceShieldGrowthRate * ForceShieldThrustGrowthRateMultiply) * DeltaTime;
			}
			else
			{
				ForceShieldRadius += ForceShieldGrowthRate * DeltaTime;
			}
			if (ForceShieldRadius >= MaxForceShieldRadius)
			{
				isForceSheildIsGrowing = false;
			}
		}
		else
		{
			if (IsThrustEnabled)
			{
				ForceShieldRadius -= (ForceShieldGrowthRate * ForceShieldThrustGrowthRateMultiply) * DeltaTime;
			}
			else
			{
				ForceShieldRadius -= ForceShieldGrowthRate * DeltaTime;
			}
			if (ForceShieldRadius <= 50) // TODO: ADD MIN RADIUS
			{
				isForceSheildIsGrowing = true;
			}
		}
		FirstPersonRadialForceComponent->Radius = ForceShieldRadius;
		ForceFieldDisplay->SetSphereRadius(ForceShieldRadius, true);

		UE_LOG(LogTemp, Warning, TEXT("Power: %f "), ForceShieldRadius);
	}
}

void AspikePe5Character::OnFire()
{
	UWorld* const World = GetWorld();
	// try and fire a projectile
	if (ProjectileClass != NULL)
	{
		if (World != NULL)
		{
			switch (CurrentWeapon)
			{
			case WeaponTypeEnum::None:

				break;
			case WeaponTypeEnum::Laser:
				AspikePe5Character::LaserBeam(World);
				break;
			case WeaponTypeEnum::Riffle:
				AspikePe5Character::BulletWeaponFire();
				break;
			case WeaponTypeEnum::ForcePush:
				AspikePe5Character::ForcePush(World);
				break;
			default:
				break;
			}

		}
	}
}

void AspikePe5Character::BulletWeaponFire()
{
	if (ProjectileClass != NULL)
	{
		UWorld* const World = GetWorld();
		if (World != NULL)
		{
			if (bUsingMotionControllers)
			{
				const FRotator SpawnRotation = VR_MuzzleLocation->GetComponentRotation();
				const FVector SpawnLocation = VR_MuzzleLocation->GetComponentLocation();
				World->SpawnActor<AspikePe5Projectile>(ProjectileClass, SpawnLocation, SpawnRotation);
			}
			else
			{
				const FRotator SpawnRotation = GetControlRotation();
				// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
				const FVector SpawnLocation = ((FP_MuzzleLocation != nullptr) ? FP_MuzzleLocation->GetComponentLocation() : GetActorLocation()) + SpawnRotation.RotateVector(GunOffset);

				//Set Spawn Collision Handling Override
				FActorSpawnParameters ActorSpawnParams;
				ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

				// spawn the projectile at the muzzle
				World->SpawnActor<AspikePe5Projectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);
			}
		}
	}

	// try and play the sound if specified
	if (FireSound != NULL)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
	}

	// try and play a firing animation if specified
	if (FireAnimation != NULL)
	{
		// Get the animation object for the arms mesh
		UAnimInstance* AnimInstance = Mesh1P->GetAnimInstance();
		if (AnimInstance != NULL)
		{
			AnimInstance->Montage_Play(FireAnimation, 1.f);
		}
	}
}


void AspikePe5Character::LaserBeam(UWorld *const &World)
{
	FCollisionQueryParams FireTraceParams;

	const FName TraceTag("MyTraceTag");

	World->DebugDrawTraceTag = TraceTag;

	FireTraceParams.bTraceComplex = false;
	FireTraceParams.bTraceAsyncScene = true;
	FireTraceParams.bReturnPhysicalMaterial = true;
	FireTraceParams.TraceTag = TraceTag;

	TArray<AActor*> ignoredActors;
	UGameplayStatics::GetAllActorsWithTag(GetWorld(), FName("Glass"), ignoredActors);

	FireTraceParams.AddIgnoredActors(ignoredActors);

	FHitResult FireHit;
	FVector StartLocation;
	const FRotator SpawnRotation = GetControlRotation();
	StartLocation = ((FP_MuzzleLocation != nullptr) ? FP_MuzzleLocation->GetComponentLocation() : GetActorLocation()) + SpawnRotation.RotateVector(GunOffset);

	FVector EndLocation;
	EndLocation = StartLocation + (FirstPersonCameraComponent->GetForwardVector() * RayDistance); // TODO Change to distance


	if (World)
	{
		bool bIsHit = World->LineTraceSingleByChannel(FireHit, StartLocation, EndLocation, ECC_Visibility, FireTraceParams);

		if (bIsHit)
		{
			UPhysicalMaterial* PhysicalMtl = FireHit.PhysMaterial.Get();

			if (PhysicalMtl->SurfaceType == SURFACE_Wood)
			{
				if (LaserSoundList[0] != NULL)
				{
					LaserSound = LaserSoundList[0];
				}
				if (DecalMaterialList[0])
				{
					DecalMaterial = DecalMaterialList[0];
				}
				else {
					DecalMaterial = NULL;
				}
			}
			else if (PhysicalMtl->SurfaceType == SURFACE_Metal)
			{
				if (LaserSoundList[1] != NULL)
				{
					LaserSound = LaserSoundList[1];
				}
				if (DecalMaterialList[1])
				{
					DecalMaterial = DecalMaterialList[1];
				}
				else {
					DecalMaterial = NULL;
				}
			}
			else if (PhysicalMtl->SurfaceType == SURFACE_Stone)
			{
				if (LaserSoundList[2] != NULL)
				{
					LaserSound = LaserSoundList[2];
				}
				if (DecalMaterialList[2])
				{
					DecalMaterial = DecalMaterialList[2];
				}
				else {
					DecalMaterial = NULL;
				}
			}

			FVector hitLocation = FireHit.ImpactPoint;

			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT(" Location x: %02f y: %02f z: %02f"), hitLocation.X, hitLocation.Y, hitLocation.Z));

			UDestructibleComponent * ComponentToDamage = Cast<UDestructibleComponent>(FireHit.GetComponent());

			if (ComponentToDamage != NULL)
			{
				float BaseDamage = 100;
				AController * EventInstigator = UGameplayStatics::GetPlayerController(World, 0);
				AActor * DamageCauser = this;
				TSubclassOf < class UDamageType > DamageTypeClass;

				ComponentToDamage->ApplyRadiusDamage(BaseDamage, FireHit.ImpactPoint, 100, 100, true);

			}
			else
			{
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString(TEXT("No Destruction")));
			}
				
			
			

			if (ParticleSystem != NULL)
			{
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ParticleSystem, hitLocation, FRotator::ZeroRotator, true);
			}

			FRotator DecalRotation = FireHit.ImpactNormal.Rotation();
			if (DecalMaterial != NULL)
			{
				//UGameplayStatics::SpawnDecalAtLocation(GetWorld(), DecalMaterial, DecalSize, hitLocation, DecalRotation, 1.50f);
				UGameplayStatics::SpawnDecalAttached(DecalMaterial,
					DecalSize,
					FireHit.GetComponent(),
					FireHit.BoneName,
					hitLocation,
					DecalRotation,
					EAttachLocation::KeepWorldPosition,
					1.50f);
			}

			// try and play the sound if specified
			if (LaserSound != NULL)
			{
				UGameplayStatics::PlaySoundAtLocation(World, LaserSound, hitLocation);
			}
			
		}
		else
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString(TEXT("No Hit")));
		}

	}
}


void AspikePe5Character::ForcePush(UWorld *const &World)
{

	TArray<FHitResult> FireHits;
	FVector StartLocation;
	FVector EndLocation;
	FCollisionObjectQueryParams ObjectQueryParams;
	ObjectQueryParams.AddObjectTypesToQuery(ECollisionChannel::ECC_PhysicsBody);
	FCollisionShape CollisionShape;
	FCollisionQueryParams Params;

	CollisionShape = FCollisionShape::MakeBox(Box);
	const FRotator SpawnRotation = GetControlRotation();
	StartLocation = ((FP_MuzzleLocation != nullptr) ? FP_MuzzleLocation->GetComponentLocation() : GetActorLocation()) + SpawnRotation.RotateVector(GunOffset);

	EndLocation = StartLocation + (FirstPersonCameraComponent->GetForwardVector() * RayDistance); // TODO Change to distance

																						  // Debug
	const FName TraceTag("MyTraceTag2");

	World->DebugDrawTraceTag = TraceTag;

	Params.TraceTag = TraceTag;
	Params.bTraceComplex = false;
	Params.bTraceAsyncScene = true;
	Params.bReturnPhysicalMaterial = true;


	if (World)
	{
		//		bool bIsHit = World->SweepMultiByObjectType(FireHits, StartLocation, EndLocation, Rot, ObjectQueryParams, CollisionShape, Params);
		bool bIsHit = World->LineTraceMultiByObjectType(FireHits, StartLocation, EndLocation, ObjectQueryParams, Params);

		if (bIsHit)
		{
			//			UPhysicalMaterial* PhysicalMtl = FireHits.PhysMaterial.Get();
			const TArray<FHitResult> Impacts = FireHits;
			for (const FHitResult& Impact : Impacts)
			{
				//UE_LOG(LogTemp, Warning, TEXT("MyCharacter's FName is %s"), *Impact.GetActor()->GetFName.ToString());
				UPrimitiveComponent* PushComponent = Impact.GetComponent();
				if (PushComponent->IsSimulatingPhysics())
				{
					UE_LOG(LogTemp, Warning, TEXT("MyCharacter's FName is: %s "), *PushComponent->GetOwner()->GetName());
					FVector ForwardVector = FirstPersonCameraComponent->GetForwardVector();
					PushComponent->AddForce(ForwardVector * ForcePower);
				}
			}
		}
		/*FVector hitLocation = FireHit.ImpactPoint;*/
	}
}


void AspikePe5Character::StartRadialFire()
{
	HasSheildGrowingStarted = true;
}

void AspikePe5Character::FireRadialFire()
{
	HasSheildGrowingStarted = false;
	ForceShieldRadius = 50; // TODO: Add Min Radius
							// Impulse
	if (FirstPersonRadialForceComponent != NULL)
	{
		FirstPersonRadialForceComponent->FireImpulse();
	}
	ForceFieldDisplay->SetHiddenInGame(true, true);
}

void AspikePe5Character::EnableThrust()
{
	if (HasSheildGrowingStarted)
	{
		IsThrustEnabled = true;
		UE_LOG(LogTemp, Warning, TEXT("Thrust Enabled"));
	}
	else
	{
		ACharacter::Jump();
	}
}

void AspikePe5Character::DisableThrust()
{
	IsThrustEnabled = false;
	ACharacter::StopJumping();
}

void AspikePe5Character::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AspikePe5Character::BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == true)
	{
		return;
	}
	TouchItem.bIsPressed = true;
	TouchItem.FingerIndex = FingerIndex;
	TouchItem.Location = Location;
	TouchItem.bMoved = false;
}

void AspikePe5Character::EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == false)
	{
		return;
	}
	if ((FingerIndex == TouchItem.FingerIndex) && (TouchItem.bMoved == false))
	{
		OnFire();
	}
	TouchItem.bIsPressed = false;
}

void AspikePe5Character::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void AspikePe5Character::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void AspikePe5Character::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AspikePe5Character::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

bool AspikePe5Character::EnableTouchscreenMovement(class UInputComponent* PlayerInputComponent)
{
	bool bResult = false;
	if (FPlatformMisc::GetUseVirtualJoysticks() || GetDefault<UInputSettings>()->bUseMouseForTouch)
	{
		bResult = true;
		PlayerInputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AspikePe5Character::BeginTouch);
		PlayerInputComponent->BindTouch(EInputEvent::IE_Released, this, &AspikePe5Character::EndTouch);

		//Commenting this out to be more consistent with FPS BP template.
		//PlayerInputComponent->BindTouch(EInputEvent::IE_Repeat, this, &AspikePe5Character::TouchUpdate);
	}
	return bResult;
}
