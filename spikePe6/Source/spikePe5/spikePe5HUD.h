// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once 
#include "GameFramework/HUD.h"
#include "spikePe5HUD.generated.h"

UCLASS()
class AspikePe5HUD : public AHUD
{
	GENERATED_BODY()

public:
	AspikePe5HUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

